package main

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
)

const tmpl = ""



func printKey() {
	fmt.Println("Starting printKey...")
	// Change to comment
	//Generate Private Key
	pvk, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(pvk)
}

func main() {
  printKey()
}
